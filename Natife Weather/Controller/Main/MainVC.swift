//
//  ViewController.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 10.05.2022.
//

import UIKit
import Bond

class MainVC: UIViewController {
	
	lazy var currentWeatherView: UIView = {
		let view = UIView()
		view.backgroundColor = UIColor(named: "regular.blue")
		return view
	}()
	
	private var hourlyWeatherForecastCollectionView: UICollectionView = {
		let collectionViewLayout = UICollectionViewFlowLayout()
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
		collectionViewLayout.scrollDirection = .horizontal
		collectionView.showsHorizontalScrollIndicator = false
		collectionView.backgroundColor = UIColor(named: "sky.blue")
		collectionView.register(MainCollectionCell.self, forCellWithReuseIdentifier: "ItemCell")
		return collectionView
	}()
	
	lazy var dailyWeatherForecastTableView: UITableView = {
		let tableView = UITableView()
		tableView.backgroundColor = UIColor(named: "regular.white")
		tableView.delegate = self
		tableView.dataSource = self
		tableView.separatorStyle = .none
		tableView.register(MainTableCell.self, forCellReuseIdentifier: "TableCell")
		return tableView
	}()
	
	lazy var placeButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "place"), for: .normal)
		button.setTitle(" Запорожье", for: .normal)
		button.titleLabel?.font = .systemFont(ofSize: 25, weight: .regular)
		button.titleLabel?.textColor = UIColor(named: "regular.white")
		button.tintColor = UIColor(named: "regular.white")
		button.addTarget(self, action: #selector(goToLocationSearchScreen(_:)), for: .touchUpInside)
		
		return button
	}()
	
	lazy var myLocationButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "my.location"), for: .normal)
		button.tintColor = UIColor(named: "regular.white")
		return button
	}()
	
	lazy var currentDateLabel: UILabel = {
		let label = UILabel()
		label.text = "ЧТ, 12 мая"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 16, weight: .regular)
		return label
	}()
	
	lazy var currentWeatherIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "white.day.cloudy"))
		iconView.tintColor = UIColor(named: "regular.white")
		iconView.contentMode = .scaleAspectFit
		return iconView
	}()
	
	lazy var temperatureIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "temp"))
		iconView.tintColor = UIColor(named: "regular.white")
		iconView.contentMode = .scaleAspectFit
		return iconView
	}()
	
	lazy var humidityIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "humidity"))
		iconView.tintColor = UIColor(named: "regular.white")
		iconView.contentMode = .scaleAspectFit
		return iconView
	}()
	
	lazy var windIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "wind"))
		iconView.tintColor = UIColor(named: "regular.white")
		iconView.contentMode = .scaleAspectFit
		return iconView
	}()
	
	lazy var windDirectionIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "wind.ne"))
		iconView.tintColor = UIColor(named: "regular.white")
		iconView.contentMode = .scaleAspectFill
		return iconView
	}()
	
	lazy var temperatureLabel: UILabel = {
		let label = UILabel()
		label.text = "27/19"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 22, weight: .regular)
		return label
	}()
	
	lazy var humidityLabel: UILabel = {
		let label = UILabel()
		label.text = "33%"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 22, weight: .regular)
		return label
	}()
	
	lazy var windLabel: UILabel = {
		let label = UILabel()
		label.text = "5м/сек"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 22, weight: .regular)
		return label
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupViews()
		setupDataToViews()
		setupConstraints()
		
		print("\(DataManager.shared.latitude), \(DataManager.shared.longitude)")
		for place in CoreDataManager.shared.place {
			print(place)
		}
		
	}
	
	func setupViews() {
		view.addSubview(currentWeatherView)
		currentWeatherView.addSubview(placeButton)
		currentWeatherView.addSubview(myLocationButton)
		currentWeatherView.addSubview(currentDateLabel)
		currentWeatherView.addSubview(currentWeatherIcon)
		currentWeatherView.addSubview(temperatureIcon)
		currentWeatherView.addSubview(humidityIcon)
		currentWeatherView.addSubview(windIcon)
		currentWeatherView.addSubview(temperatureLabel)
		currentWeatherView.addSubview(humidityLabel)
		currentWeatherView.addSubview(windLabel)
		currentWeatherView.addSubview(windDirectionIcon)
		
		view.addSubview(hourlyWeatherForecastCollectionView)
		hourlyWeatherForecastCollectionView.delegate = self
		hourlyWeatherForecastCollectionView.dataSource = self
		
		view.addSubview(dailyWeatherForecastTableView)
	}
	
	func setupDataToViews() {
		placeButton.setTitle(" " + (CoreDataManager.shared.place.first?.city ?? GlobalDataProperties.shared.currentPlace), for: .normal)
//		placeButton.reactive.base.setTitle(" " + DataManager.shared.city, for: .normal)
		currentDateLabel.text = DateFormatManager.shared.currentDate
		currentWeatherIcon.image = UIImage(named: "white.day." + DataManager.shared.currentIconName)
		temperatureLabel.text = "\(DataManager.shared.currentTemperature)°"
		humidityLabel.text = "\(DataManager.shared.currentHumidity)%"
		windLabel.text = "\(DataManager.shared.currentWindSpeed)m/sec"
	}
	
	func setupConstraints() {
		currentWeatherView.snp.makeConstraints { make in
			make.height.equalTo(290)
			make.top.left.right.equalTo(view)
		}
		placeButton.snp.makeConstraints { make in
			make.height.equalTo(25)
			make.top.equalTo(currentWeatherView).offset(35)
			make.left.equalTo(currentWeatherView).offset(20)
		}
		myLocationButton.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 25, height: 25))
			make.top.equalTo(currentWeatherView).offset(35)
			make.right.equalTo(currentWeatherView).offset(-20)
		}
		currentDateLabel.snp.makeConstraints { make in
			make.top.equalTo(placeButton.snp.bottom).offset(15)
			make.left.equalTo(currentWeatherView).offset(20)
		}
		currentWeatherIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 160, height: 160))
			make.top.equalTo(currentDateLabel.snp.bottom).offset(20)
			make.left.equalTo(currentWeatherView).offset(30)
		}
		temperatureIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 22, height: 22))
			make.top.equalTo(currentWeatherIcon).offset(27)
			make.left.equalTo(currentWeatherIcon.snp.right).offset(20)
		}
		humidityIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 22, height: 22))
			make.top.equalTo(temperatureIcon.snp.bottom).offset(17)
			make.left.equalTo(currentWeatherIcon.snp.right).offset(20)
		}
		windIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 22, height: 22))
			make.top.equalTo(humidityIcon.snp.bottom).offset(17)
			make.left.equalTo(currentWeatherIcon.snp.right).offset(20)
		}
		temperatureLabel.snp.makeConstraints { make in
			make.height.equalTo(25)
			make.centerY.equalTo(temperatureIcon)
			make.left.equalTo(temperatureIcon.snp.right).offset(10)
		}
		humidityLabel.snp.makeConstraints { make in
			make.height.equalTo(25)
			make.centerY.equalTo(humidityIcon)
			make.left.equalTo(humidityIcon.snp.right).offset(10)
		}
		windLabel.snp.makeConstraints { make in
			make.height.equalTo(25)
			make.centerY.equalTo(windIcon)
			make.left.equalTo(windIcon.snp.right).offset(10)
		}
		windDirectionIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 35, height: 20))
			make.bottom.equalTo(windLabel)
			make.left.equalTo(windLabel.snp.right)
		}
		
		hourlyWeatherForecastCollectionView.snp.makeConstraints { make in
			make.height.equalTo(150)
			make.left.right.equalTo(view)
			make.top.equalTo(currentWeatherView.snp.bottom)
		}
		
		dailyWeatherForecastTableView.snp.makeConstraints { make in
			make.bottom.left.right.equalTo(view)
			make.top.equalTo(hourlyWeatherForecastCollectionView.snp.bottom)
		}
	}
	
	@objc func goToLocationSearchScreen(_ sender: UIButton) {
		let locationSearchVC = LocationSearchVC()
		locationSearchVC.modalPresentationStyle = .overCurrentContext
		present(locationSearchVC, animated: true)
	}
}

extension MainVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 75, height: 150)
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return CoreDataManager.shared.hourlyWeather.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! MainCollectionCell
		cell.timeLabel.text = DateFormatManager.shared.dayHours[indexPath.item]
		cell.weatherIcon.image = UIImage(named: "white.day." + DataManager.shared.hourlyIconName[indexPath.item])
		cell.degreesLabel.text = "\(DataManager.shared.hourlyTemperature[indexPath.item])°"
		return cell
	}
	
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 65
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return CoreDataManager.shared.dailyWeather.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! MainTableCell
		cell.dayOfTheWeekLabel.text = DateFormatManager.shared.daysWeek[indexPath.row]
		cell.degreesMaxMinLabel.text = "\(DataManager.shared.dailyMaxTemperature[indexPath.row])°/\(DataManager.shared.dailyMinTemperature[indexPath.row])°"
		cell.weatherIcon.image = UIImage(named: "white.day." + DataManager.shared.dailyIconName[indexPath.row])
		return cell
	}


}
