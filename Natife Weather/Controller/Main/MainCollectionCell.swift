//
//  MainCollectionCell.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 13.05.2022.
//

import UIKit

class MainCollectionCell: UICollectionViewCell {
	let itemView: UIView = {
		let view = UIView()
		view.backgroundColor = .clear
		return view
	}()
	
	let timeLabel: UILabel = {
		let label = UILabel()
		label.text = "17:00"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 18, weight: .regular)
		return label
	}()
	
	let weatherIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "white.day.cloudy"))
		iconView.tintColor = UIColor(named: "regular.white")
		return iconView
	}()
	
	let degreesLabel: UILabel = {
		let label = UILabel()
		label.text = "27"
		label.textColor = UIColor(named: "regular.white")
		label.font = .systemFont(ofSize: 22, weight: .regular)
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
		setupConstraints()
	}
	
	func setupViews() {
		addSubview(itemView)
		itemView.addSubview(timeLabel)
		itemView.addSubview(weatherIcon)
		itemView.addSubview(degreesLabel)
	}
	
	func setupConstraints() {
		itemView.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 75, height: 110))
			make.centerY.equalToSuperview()
		}
		
		timeLabel.snp.makeConstraints { make in
			make.top.equalTo(itemView)
			make.centerX.equalTo(itemView)
		}
		
		weatherIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 40, height: 40))
			make.centerX.equalTo(itemView)
			make.bottom.equalTo(degreesLabel.snp.top)
		}
		
		degreesLabel.snp.makeConstraints { make in
			make.bottom.equalTo(itemView)
			make.centerX.equalTo(itemView)
		}
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
