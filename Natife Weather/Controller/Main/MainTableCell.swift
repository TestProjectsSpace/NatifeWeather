//
//  MainTableCell.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 13.05.2022.
//

import UIKit

class MainTableCell: UITableViewCell {
	
	let dayOfTheWeekLabel: UILabel = {
		let label = UILabel()
		label.text = "ПТ"
		label.textColor = UIColor(named: "regular.black")
		label.font = .systemFont(ofSize: 22, weight: .medium)
		return label
	}()
	
	let degreesMaxMinLabel: UILabel = {
		let label = UILabel()
		label.text = "27/19"
		label.textColor = UIColor(named: "regular.black")
		label.font = .systemFont(ofSize: 22, weight: .medium)
		return label
	}()
	
	let weatherIcon: UIImageView = {
		let iconView = UIImageView(image: UIImage(named: "white.day.cloudy"))
		iconView.tintColor = UIColor(named: "regular.black")
		return iconView
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupViews()
		setupConstraints()
		self.selectionStyle = .none
	}
	
	func setupViews() {
		contentView.addSubview(dayOfTheWeekLabel)
		contentView.addSubview(degreesMaxMinLabel)
		contentView.addSubview(weatherIcon)
	}
	
	func setupConstraints() {
		dayOfTheWeekLabel.snp.makeConstraints { make in
			make.left.equalTo(contentView).offset(20)
			make.centerY.equalTo(contentView)
		}
		
		degreesMaxMinLabel.snp.makeConstraints { make in
			make.center.equalTo(contentView)
		}
		
		weatherIcon.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 40, height: 40))
			make.right.equalTo(contentView).offset(-20)
			make.centerY.equalTo(contentView)
		}
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
