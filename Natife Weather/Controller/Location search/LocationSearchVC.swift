//
//  LocationSearchVC.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 13.05.2022.
//

import UIKit

class LocationSearchVC: UIViewController {
	
	lazy var locationSearchView: UIView = {
		let view = UIView()
		view.backgroundColor = UIColor(named: "regular.blue")
		return view
	}()
	
	lazy var locationSearchTableView: UITableView = {
		let tableView = UITableView()
		tableView.backgroundColor = UIColor(named: "regular.white")
		tableView.separatorStyle = .none
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(LocationSearchCell.self, forCellReuseIdentifier: "cell")
		return tableView
	}()
	
	lazy var backButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "back"), for: .normal)
		button.tintColor = UIColor(named: "regular.white")
		button.contentMode = .scaleAspectFit
		button.addTarget(self, action: #selector(backToPreviousScreen(_:)), for: .touchUpInside)
		return button
	}()
	
	lazy var searchBar: UISearchBar = {
		let searchBar = UISearchBar()
		searchBar.searchBarStyle = .minimal
		searchBar.searchTextField.backgroundColor = UIColor(named: "regular.white")
		searchBar.placeholder = "Typing the city, state or country"
		searchBar.tintColor = UIColor(named: "regular.black")
		searchBar.delegate = self
		return searchBar
	}()
	
	lazy var searchButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "search"), for: .normal)
		button.tintColor = UIColor(named: "regular.white")
		button.contentMode = .scaleAspectFit
		return button
	}()

    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		setupConstraints()
    }
	
	func tableViewDataUpdater() {
		locationSearchTableView.reloadData()
		locationSearchTableView.endUpdates()
	}
	
	func setupViews() {
		view.addSubview(locationSearchView)
		locationSearchView.addSubview(backButton)
		locationSearchView.addSubview(searchBar)
		locationSearchView.addSubview(searchButton)
		
		view.addSubview(locationSearchTableView)
	}
	
	func setupConstraints() {
		locationSearchView.snp.makeConstraints { make in
			make.height.equalTo(75)
			make.top.left.right.equalTo(view)
		}
		
		backButton.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 25, height: 25))
			make.top.equalTo(locationSearchView).offset(35)
			make.left.equalTo(locationSearchView).offset(10)
		}
		
		searchBar.snp.makeConstraints { make in
			make.centerY.equalTo(backButton)
			make.left.equalTo(backButton.snp.right)
			make.right.equalTo(searchButton.snp.left)
		}
		
		searchButton.snp.makeConstraints { make in
			make.size.equalTo(CGSize(width: 25, height: 25))
			make.top.equalTo(locationSearchView).offset(35)
			make.right.equalTo(locationSearchView).offset(-10)
		}
		
		locationSearchTableView.snp.makeConstraints { make in
			make.top.equalTo(locationSearchView.snp.bottom)
			make.left.right.bottom.equalTo(view)
		}
	}
	
	@objc func backToPreviousScreen(_ sender: UIButton) {
		dismiss(animated: true)
	}
}

extension LocationSearchVC: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		GlobalDataProperties.shared.searchBarText = searchText
		GlobalDataProperties.shared.searchIsActive = true
		tableViewDataUpdater()
		
	}
	
	func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
		searchBar.setShowsCancelButton(true, animated: true)
		if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
			cancelButton.setTitle("", for: .normal)
			cancelButton.setImage(UIImage(named: "close"), for: .normal)
			cancelButton.tintColor = UIColor(named: "regular.white")
		}
		return true
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.setShowsCancelButton(false, animated: true)
		searchBar.resignFirstResponder()
	}

}

extension LocationSearchVC: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 50
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if GlobalDataProperties.shared.searchBarText != "" {
			NetworkManager.shared.getWeatherByPlace { lat in
				GlobalDataProperties.shared.lat = lat
			} lon: { lon in
				GlobalDataProperties.shared.lon = lon
			} city: { city in
				GlobalDataProperties.shared.cities = city
			} state: { state in
				GlobalDataProperties.shared.states = state
			} country: { country in
				GlobalDataProperties.shared.countries = country
			}
		}
		return GlobalDataProperties.shared.cities.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationSearchCell
		cell.resultOfSearch.text = GlobalDataProperties.shared.cities[indexPath.row] + " / " + GlobalDataProperties.shared.states[indexPath.row] + " / " + GlobalDataProperties.shared.countries[indexPath.row]
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		CoreDataManager.shared.removingPlaceData()
		dismiss(animated: true) {
			CoreDataManager.shared.savingPlace(GlobalDataProperties.shared.cities[indexPath.row],
											   GlobalDataProperties.shared.lat[indexPath.row],
											   GlobalDataProperties.shared.lon[indexPath.row])
		}
	}
}
