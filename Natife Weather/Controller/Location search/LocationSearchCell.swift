//
//  LocationSearchCell.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 13.05.2022.
//

import UIKit

class LocationSearchCell: UITableViewCell {
	
	let resultOfSearch: UILabel = {
		let label = UILabel()
		label.text = "City Name. Country name"
		label.textColor = UIColor(named: "regular.black")
		label.font = .systemFont(ofSize: 20, weight: .regular)
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		self.selectionStyle = .none
		setupViews()
		setupConstraints()
	}
	
	func setupViews() {
		contentView.addSubview(resultOfSearch)
	}
	
	func setupConstraints() {
		resultOfSearch.snp.makeConstraints { make in
			make.center.equalTo(contentView)
		}
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
