//
//  GlobalDataProperties.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 23.05.2022.
//

import Foundation

class GlobalDataProperties {
	static var shared = GlobalDataProperties()
	
	public var currentPlace: String = "Запорожье"
	public var latitude: String = "47.8507859"
	public var longitude: String = "35.1182867"
	
	public var lat = [String]()
	public var lon = [String]()
	public var cities = [String]()
	public var states = [String]()
	public var countries = [String]()
	
	public var searchBarText = String()
	public var searchIsActive = Bool()
}
