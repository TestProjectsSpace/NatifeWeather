//
//  DateFormatManager.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 18.05.2022.
//

import Foundation

class DateFormatManager {
	static var shared = DateFormatManager()
	
	public var currentDate: String {
		var unixDate = Int()
		let formatter = DateFormatter()
		for value in CoreDataManager.shared.currentWeather {
			unixDate = Int(value.dateTime ?? "")!
		}
		formatter.dateFormat = "E, d MMM"
		return formatter.string(from: Date(timeIntervalSince1970: TimeInterval(unixDate)))
	}
	
	public var dayHours: [String] {
		var decodedUnixDate = [String]()
		let formatter = DateFormatter()
		for value in CoreDataManager.shared.hourlyWeather {
			let unix = Int(value.dateTime ?? "")!
			let date = Date(timeIntervalSince1970: TimeInterval(unix))
			formatter.dateFormat = "HH:mm"
			decodedUnixDate.append(formatter.string(from: date))
		}
		return decodedUnixDate
	}
	
	public var daysWeek: [String] {
		var decodedUnixDate = [String]()
		let formatter = DateFormatter()
		for value in CoreDataManager.shared.dailyWeather {
			let unix = Int(value.weekDay ?? "")!
			let date = Date(timeIntervalSince1970: TimeInterval(unix))
			formatter.dateFormat = "E"
			decodedUnixDate.append(formatter.string(from: date))
		}
		return decodedUnixDate
	}
}
