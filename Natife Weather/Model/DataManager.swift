//
//  DataManager.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 17.05.2022.
//

import Foundation

class DataManager {
	static var shared = DataManager()
	
	// MARK: Place
	public var city: String {
		var string = String()
		for value in CoreDataManager.shared.place {
			string = value.city ?? ""
		}
		return string
	}
	
	public var latitude: String {
		var string = String()
		for value in CoreDataManager.shared.place {
			string = value.latitude ?? ""
		}
		return string
	}
	
	public var longitude: String {
		var string = String()
		for value in CoreDataManager.shared.place {
			string = value.longitude ?? ""
		}
		return string
	}
	
	// MARK: Current weather
	public var currentIconName: String {
		var string = String()
		for value in CoreDataManager.shared.currentWeather {
			string = value.iconName ?? ""
		}
		return string
	}
	
	public var currentTemperature: Int {
		var integer = Int()
		for value in CoreDataManager.shared.currentWeather {
			integer = Int(value.temperature)
		}
		return integer
	}
	
	public var currentHumidity: Int {
		var integer = Int()
		for value in CoreDataManager.shared.currentWeather {
			integer = Int(value.humidity)
		}
		return integer
	}
	
	public var currentWindSpeed: Int {
		var integer = Int()
		for value in CoreDataManager.shared.currentWeather {
			integer = Int(value.windSpeed)
		}
		return integer
	}
	
	// MARK: Hourly weather
	public var hourlyDateTime: [String] {
		var array = [String]()
		for value in CoreDataManager.shared.hourlyWeather {
			array.append(value.dateTime ?? "")
		}
		return array
	}
	
	public var hourlyIconName: [String] {
		var array = [String]()
		for value in CoreDataManager.shared.hourlyWeather {
			array.append(value.iconName ?? "")
		}
		return array
	}
	
	public var hourlyTemperature: [Int] {
		var array = [Int]()
		for value in CoreDataManager.shared.hourlyWeather {
			array.append(Int(value.temperature))
		}
		return array
	}
	
	// MARK: Daily weather
	public var dailyDayTime: [String] {
		var array = [String]()
		for value in CoreDataManager.shared.dailyWeather {
			array.append(value.weekDay ?? "")
		}
		return array
	}
	
	public var dailyIconName: [String] {
		var array = [String]()
		for value in CoreDataManager.shared.dailyWeather {
			array.append(value.iconName ?? "")
		}
		return array
	}
	
	public var dailyMaxTemperature: [Int] {
		var array = [Int]()
		for value in CoreDataManager.shared.dailyWeather {
			array.append(Int(value.maxTemperature))
		}
		return array
	}
	
	public var dailyMinTemperature: [Int] {
		var array = [Int]()
		for value in CoreDataManager.shared.dailyWeather {
			array.append(Int(value.minTemperature))
		}
		return array
	}
}
