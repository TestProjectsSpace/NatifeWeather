//
//  CoreDataManager.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 17.05.2022.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
	static var shared = CoreDataManager()
	private let context = ((UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext)!
	
	// MARK: Core Data properties
	public var place = [Place]()
	public var currentWeather = [Current]()
	public var hourlyWeather = [Hourly]()
	public var dailyWeather = [Daily]()
	
	// MARK: Core Data methods to saving data to persistent storage
	public func savingPlace(_ city: String, _ latitude: String, _ longitude: String) {
		let currentPlace = Place(context: context)
		currentPlace.city = city
		currentPlace.latitude = latitude
		currentPlace.longitude = longitude
		
		do {
			try context.save()
			place.append(currentPlace)
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
	
	public func savingCurrentWeatherData(_ dateTime: String, _ iconName: String, _ temperature: Int16, _ humidity: Int16, _ windSpeed: Int16, _ windDegrees: Int16) {
		let current = Current(context: context)
		current.dateTime = dateTime
		current.iconName = iconName
		current.temperature = temperature
		current.humidity = humidity
		current.windSpeed = windSpeed
		current.windDegrees = windDegrees
		
		do {
			try context.save()
			currentWeather.append(current)
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
	
	public func savingHourlyWeatherData(_ dateTime: String, _ iconName: String, _ temperature: Int16) {
		let hourly = Hourly(context: context)
		hourly.dateTime = dateTime
		hourly.temperature = temperature
		hourly.iconName = iconName
		
		do {
			try context.save()
			hourlyWeather.append(hourly)
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
	
	public func savingDailyWeatherData(_ weekDay: String, _ iconName: String, _ maxTemperature: Int16, _ minTemperature: Int16) {
		let daily = Daily(context: context)
		daily.weekDay = weekDay
		daily.iconName = iconName
		daily.maxTemperature = maxTemperature
		daily.minTemperature = minTemperature
		
		do {
			try context.save()
			dailyWeather.append(daily)
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
	
	// MARK: Core Data methods for retriving data from persistent storage
	public func retrivingPlaceData() {
		let fetchingRequestPlaceData: NSFetchRequest<Place> = Place.fetchRequest()
		do {
			place = try context.fetch(fetchingRequestPlaceData)
		} catch let error as NSError {
			print("Could not fetch. \(error), \(error.userInfo)")
		}
	}
	
	public func retrivingData() {
		let fetchingRequestCurrentWeatherData: NSFetchRequest<Current> = Current.fetchRequest()
		let fetchingRequestHourlyWeatherData: NSFetchRequest<Hourly> = Hourly.fetchRequest()
		let fetchingRequestDailyWeatherData: NSFetchRequest<Daily> = Daily.fetchRequest()
		do {
			currentWeather = try context.fetch(fetchingRequestCurrentWeatherData)
			hourlyWeather = try context.fetch(fetchingRequestHourlyWeatherData)
			dailyWeather = try context.fetch(fetchingRequestDailyWeatherData)
		} catch let error as NSError {
			print("Could not fetch. \(error), \(error.userInfo)")
		}
	}
	
	// MARK: Core Data methods for removing data from persistent storage
	public func removingPlaceData() {
		let fetchingRequestPlaceData: NSFetchRequest<NSFetchRequestResult> = Place.fetchRequest()
		let removePlaceData = NSBatchDeleteRequest(fetchRequest: fetchingRequestPlaceData)
		
		do {
			try context.execute(removePlaceData)
			place.removeAll()
		} catch let error as NSError {
			print("Could not delete. \(error), \(error.userInfo)")
		}
	}
	
	public func removingCurrentWeatherData() {
		let fetchingRequestCurrentWeatherData: NSFetchRequest<NSFetchRequestResult> = Current.fetchRequest()
		let removeCurrentWeatherData = NSBatchDeleteRequest(fetchRequest: fetchingRequestCurrentWeatherData)
		
		do {
			try context.execute(removeCurrentWeatherData)
			currentWeather.removeAll()
		} catch let error as NSError {
			print("Could not delete. \(error), \(error.userInfo)")
		}
	}
	
	public func removingHourlyWeatherData() {
		let fetchingRequestHourlyWeatherData: NSFetchRequest<NSFetchRequestResult> = Hourly.fetchRequest()
		let removeHourlyWeatherData = NSBatchDeleteRequest(fetchRequest: fetchingRequestHourlyWeatherData)
		
		do {
			try context.execute(removeHourlyWeatherData)
			hourlyWeather.removeAll()
		} catch let error as NSError {
			print("Could not delete. \(error), \(error.userInfo)")
		}
	}
	
	public func removingDailyWeatherData() {
		let fetchingRequestDailyWeatherData: NSFetchRequest<NSFetchRequestResult> = Daily.fetchRequest()
		let removeDailyWeatherData = NSBatchDeleteRequest(fetchRequest: fetchingRequestDailyWeatherData)
		
		do {
			try context.execute(removeDailyWeatherData)
			dailyWeather.removeAll()
		} catch let error as NSError {
			print("Could not delete. \(error), \(error.userInfo)")
		}
	}
}
