//
//  NetworkManager.swift
//  Natife Weather
//
//  Created by Nikolay Goncharenko on 13.05.2022.
//

import Alamofire
import Foundation
import SwiftyJSON

class NetworkManager {
	static var shared = NetworkManager()
	
	// MARK: Prorerties for creating Network Requests
	private func oneCallRequest(_ lat: String, _ lon: String) -> DataRequest {
		let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall")!
		let urlParams = [
			"lat":lat,
			"lon":lon,
			"units":"metric",
			"exclude":"minutely",
			"appid":"ec7a71a0b86c79eb2f01a230ac62e06f"
		]
		let request = AF.request(url, method: .get, parameters: urlParams)
		return request.validate(statusCode: 200..<300)
	}
	
	private func geocodingRequest(_ q: String) -> DataRequest {
		let url = URL(string: "https://api.openweathermap.org/geo/1.0/direct")!
		let urlParams = [
			"q":q,
			"limit":"5",
			"appid":"ec7a71a0b86c79eb2f01a230ac62e06f"
		]
		let request = AF.request(url, method: .get, parameters: urlParams)
		return request.validate(statusCode: 200..<300)
	}
	
	// MARK: Methods for creating Network Requests
	public func currentWeather() {
		oneCallRequest(CoreDataManager.shared.place.first?.latitude ?? GlobalDataProperties.shared.latitude, CoreDataManager.shared.place.first?.longitude ?? GlobalDataProperties.shared.longitude).responseData { response in
				switch response.result {
					case .success(let value):
						let json = JSON(value).dictionaryValue
						let current = json["current"]
						let dateTime = current!["dt"].stringValue
						let temperature = current!["temp"].int16Value
						let humidity = current!["humidity"].int16Value
						let windSpeed = current!["wind_speed"].int16Value
						let windDegrees = current!["wind_deg"].int16Value
						let iconName = current!["weather"][0]["main"].stringValue.description.lowercased()
						
						switch CoreDataManager.shared.currentWeather.isEmpty {
							case true:
								CoreDataManager.shared.savingCurrentWeatherData(dateTime, iconName, temperature, humidity, windSpeed, windDegrees)
							case false:
								CoreDataManager.shared.removingCurrentWeatherData()
								CoreDataManager.shared.savingCurrentWeatherData(dateTime, iconName, temperature, humidity, windSpeed, windDegrees)
						}
						
					case .failure(let error):
						print(error.localizedDescription)
				}
			}
	}
	
	public func hourlyForecastWeather() {
		oneCallRequest(CoreDataManager.shared.place.first?.latitude ?? GlobalDataProperties.shared.latitude, CoreDataManager.shared.place.first?.longitude ?? GlobalDataProperties.shared.longitude).responseData { response in
			switch response.result {
				case .success(let value):
					let json = JSON(value)
					let hourly = json["hourly"].arrayValue
					CoreDataManager.shared.removingHourlyWeatherData()
					for (index, _) in hourly.enumerated() {
						let dateTime = hourly[index]["dt"].stringValue
						let iconName = hourly[index]["weather"].arrayValue[0]["main"].stringValue.lowercased()
						let temperature = hourly[index]["temp"].int16Value
						
						CoreDataManager.shared.savingHourlyWeatherData(dateTime, iconName, temperature)
					}
					
				case .failure(let error):
					print(error.localizedDescription)
			}
		}
	}
	
	public func dailyForecastWeather() {
		oneCallRequest(CoreDataManager.shared.place.first?.latitude ?? GlobalDataProperties.shared.latitude, CoreDataManager.shared.place.first?.longitude ?? GlobalDataProperties.shared.longitude).responseData { response in
			switch response.result {
				case .success(let value):
					let json = JSON(value)
					let daily = json["daily"].arrayValue
					CoreDataManager.shared.removingDailyWeatherData()
					for (index, _) in daily.enumerated() {
						let weekDay = daily[index]["dt"].stringValue
						let iconName = daily[index]["weather"][0]["main"].stringValue.lowercased()
						let maxTemperature = daily[index]["temp"]["max"].int16Value
						let minTemperature = daily[index]["temp"]["min"].int16Value
						
						CoreDataManager.shared.savingDailyWeatherData(weekDay, iconName, maxTemperature, minTemperature)
					}
				case .failure(let error):
					print(error.localizedDescription)
			}
		}
	}
	
	public func getWeatherByPlace(lat: @escaping ([String]) -> Void, lon: @escaping ([String]) -> Void, city: @escaping ([String]) -> Void, state: @escaping ([String]) -> Void, country: @escaping ([String]) -> Void) {
		geocodingRequest(GlobalDataProperties.shared.searchBarText).responseData { response in
			switch response.result {
				case .success(let value):
					let json = JSON(value).arrayValue
					var latitude = [String]()
					var longitude = [String]()
					var citySet = [String]()
					var stateSet = [String]()
					var countrySet = [String]()
					for (index, _) in json.enumerated() {
						latitude.append(json[index]["lat"].stringValue)
						longitude.append(json[index]["lon"].stringValue)
						citySet.append(json[index]["name"].stringValue)
						stateSet.append(json[index]["state"].string ?? "State not found")
						countrySet.append(json[index]["country"].stringValue)
					}
					lat(latitude)
					lon(longitude)
					city(citySet)
					state(stateSet)
					country(countrySet)
				case .failure(let error):
					print(error)
			}
		}
	}
}
